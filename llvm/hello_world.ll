; the \0A is a newline, read an ascii table. `man ascii`
@.str = private unnamed_addr constant [12 x i8] c"hello world\0A"

; if it begins with __ it's an assembly routine, otherwise see libutil.ll
declare i32 @__putstr(i8* nocapture,i8) nounwind
declare void @__exit() nounwind
declare void @nl() nounwind
declare i32 @print_single(i8) nounwind

; Definition of main function
define i32 @main() {  
        ;calculates address of the first element in @.str
	%cast210 = getelementptr [12 x i8], [12 x i8]* @.str, i64 0, i64 0
        ; calls the putstr function
	call i32 @__putstr(i8* %cast210,i8 12)
	ret i32 0
}
