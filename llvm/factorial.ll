; the \0A is a newline, read an ascii table. `man ascii`
@.str = private unnamed_addr constant [12 x i8] c"hello world\0A"

; if it begins with __ it's an assembly routine, otherwise see libutil.ll
declare i32 @__putstr(i8* nocapture,i8) nounwind
declare void @__exit() nounwind
declare void @nl() nounwind
declare i32 @print_single(i8) nounwind
declare i32 @intprint(i32) nounwind

; Definition of main function
define i32 @main() {  
  %y = call i32 @factorial(i32 10)
  call i32 @intprint(i32 %y)
  call void @nl()
  ret i32 0
}

define i32 @factorial(i32 %n) {
  %cond = icmp eq i32 %n, 1
  br i1 %cond, label %done,label %notd
notd:
  %y = sub i32 %n,1
  %1 = call i32 @factorial(i32 %y)
  %2 = mul i32 %1,%n
  ret i32 %2

done:
  ret i32 1
}
