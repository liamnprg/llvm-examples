; Declare the string constant as a global constant.
@.str = private unnamed_addr constant [18 x i8] c"This is a number: "

; External declaration of the puts function
declare i32 @__putstr(i8* nocapture,i8) nounwind
declare void @nl() nounwind
declare void @__exit() nounwind
declare i32 @__print_single(i8* nocapture) nounwind
declare i32 @print_single(i8) nounwind
declare i32 @intprint(i32) nounwind
declare i32 @pow(i32,i32) nounwind

; Definition of main function
define i32 @main() {  
        ;calculates address of the first element in @.str
	%cast210 = getelementptr [18 x i8], [18 x i8]* @.str, i64 0, i64 0
	call i32 @__putstr(i8* %cast210,i8 18)
        call i32 @intprint(i32 1337)
        call void @nl() 
	ret i32 0
}
