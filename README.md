# llvm-examples

This is a code example for a no-runtime-dependencies batch of llvm programs. These programs use custom assembly routines for interfacing with the operating system.

## Warning

The makefile makes use of pre-compiled llc/as and possibly more, if you don't like that, adjust ir-shared using your own binaries

## Todo

- add more architectures when I learn them (Arm,Mips)
- handle signs in intprint(not the \_i function, the actual called function), don't think the int's are signed
- add link to ir-shared/llvm-examples in git clone instructions in ir-shared and this repo


## Using

### Deps

You need an LLVM that supports RISC-V(if you are building for risc-v), and a riscv LINUX toolchain alongise a proper installation of ld and binutils

BE SURE to git clone https://gitlab.com/liamnprg/ir-shared.git into the same directory as the README.md file you are reading

run `make`(in the current directory) to build ir-shared, this is needed for the ir-comp script to work in the llvm subdirectory

## Project structure

### ir-shared

Please consult the [ir-shared](https://gitlab.com/liamnprg/ir-shared) repository for more information on ir-shared

### llvm

This contains samples of llvm ir code stored in .ll files. The ir-comp script handles compilation and linking. It requires that ir-shared is compiled either via the top level makefile(`./Makefile`), or by `ir-shared/Makefile. 

In order to use ir-comp, you need an arch(rv64/32,x64/32), and a .ll file to compile. Here is an example: `./ir-comp x64 hello.ll` 


## Why am I doing this

I am writing an llvm based compiler, and I needed to learn how to program in llvm ir. ir-shared will be my runtime.
