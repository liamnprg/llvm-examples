all:
	@echo "/////////////////////////////////////////////////////////////////"
	@echo "/ Welcome to llvm-tut! I hope you enjoy these code examples     /"
	@echo "/ Read the readme please, it contains very valuable information /"
	@echo "/////////////////////////////////////////////////////////////////"
	@echo "This program will download and compile the following things: libnative, a platform specific assembly module, and ir-shared, a bunch of precompiled binaries for working with wierd architectures like riscv. Read the readme for more info"
	cd ir-shared && make


clean:
	cd ir-shared && make clean
